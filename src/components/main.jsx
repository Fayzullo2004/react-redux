import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Loader} from "../UI";
import {getArticlesStart, getArticlesSuccess} from "../slice/article";
import ArticleService from "../service/article";
import {ArticleCard} from "./index";

function Main() {
  const {articles, isLoading} = useSelector(state => state.article)
  const dispatch = useDispatch()

  const getArticles = async () => {
    dispatch(getArticlesStart())
    try {
      const response = await ArticleService.getArticles()
      dispatch(getArticlesSuccess(response.articles))
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    getArticles()
  }, [])

  return (
    <>
      {isLoading && <Loader/>}
      <div className="album py-5 ">
        <div className="">
          <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
            {articles.map((item, idx) => (
              <ArticleCard idx={idx} item={item} getArticles={getArticles}  key={idx}/>
            ))}
          </div>
        </div>
      </div>
    </>
  );
}

export default Main;
