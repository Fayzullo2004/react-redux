import {Background} from "../constants";
import {useNavigate} from "react-router-dom";
import {useSelector} from "react-redux";
import ArticleService from "../service/article";

function ArticleCard({item, idx, getArticles}) {
  const {loggedIn, user} = useSelector(state => state.auth)
  const navigate = useNavigate()

  const deleteArticle = async (slug) => {
    try {
      await ArticleService.deleteArticle(slug)
      getArticles()
    } catch (error) {

    }
  }

  return (
    <div className="col">
      <div className="card shadow-sm h-100">
        <Background/>
        <div className="card-body">
          <p className="card-text fw-bold m-0">{item.title}</p>
          <p className="card-text ">{item.description}</p>
        </div>
        <div className="d-flex card-footer justify-content-between align-items-center">
          <div className="btn-group">
            <button onClick={() => navigate(`/articles/${item.slug}`)} type="button"
                    className="btn btn-sm btn-outline-success">
              View
            </button>
            {loggedIn && user.username === item.author.username && (
              <>
                <button
                  type="button"
                  className="btn btn-sm btn-outline-secondary"
                  onClick={() => navigate(`/edit-article/${item.slug}`)}
                >
                  Edit
                </button>
                <button
                  type="button"
                  className="btn btn-sm btn-outline-danger"
                  onClick={() => deleteArticle(item.slug)}
                >
                  Delete
                </button>
              </>
            )}
          </div>
          <small className="text-body-secondary fw-bold text-copitalize">
            {item.author.username}
          </small>
        </div>
      </div>
    </div>

  );
}

export default ArticleCard;