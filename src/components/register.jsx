import {Logo} from "../constants";
import {Input} from "../UI";
import {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {singUserFailure, singUserStart, singUserSuccess} from "../slice/auth";
import AuthService from "../service/auth";
import {ValidationError} from "./index";
import {useNavigate} from "react-router-dom";

function Register() {
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const dispatch = useDispatch()
  const {loggedIn} = useSelector(state => state.auth)
  const navigate = useNavigate()

  const registerHandler = async (e) => {
    e.preventDefault()
    dispatch(singUserStart())
    const user = {
      username: name,
      email,
      password
    }
    try {
      const response = await AuthService.userRegister(user)
      dispatch(singUserSuccess(response.user))
      navigate('/')
    } catch (error) {
      dispatch(singUserFailure(error.response.data.errors))
      console.log(error.response.data)
    }
  }

  useEffect(() => {
    if (loggedIn) {
      navigate('/')
    }
  }, [loggedIn])

  return (
    <div className='text-center mt-5'>
      <main className="form-signin w-25 m-auto">
        <form>
          <Logo/>
          <h1 className="h3 mb-3 fw-normal">Please register</h1>
          <ValidationError/>
          <Input label={'Username'} state={name} setState={setName}/>
          <Input label={'Email address'} state={email} setState={setEmail}/>
          <Input label={'Password'} type={'password'} state={password} setState={setPassword}/>

          <button
            className="w-100 btn btn-lg btn-primary"
            type="submit"
            onClick={registerHandler}
          >
            register
          </button>
        </form>
      </main>
    </div>
  );
}

export default Register;