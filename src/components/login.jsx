import {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useNavigate} from "react-router-dom";
import AuthService from "../service/auth";
import {singUserFailure, singUserStart, singUserSuccess} from "../slice/auth";
import {Logo} from "../constants";
import {ValidationError} from "./index";
import {Input} from "../UI";

function Login() {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const dispatch = useDispatch()
  const {loggedIn} = useSelector(state => state.auth)
  const navigate = useNavigate()

  const loginHandler = async (e) => {
    e.preventDefault()
    dispatch(singUserStart())
    const user = {
      email,
      password
    }
    try {
      const response = await AuthService.userLogin(user)
      dispatch(singUserSuccess(response.user))
      navigate('/')
    } catch (error) {
      dispatch(singUserFailure(error.response.data.errors))
    }
  }

  useEffect(() => {
    if (loggedIn) {
      navigate('/')
    }
  }, [loggedIn])

  return (
    <div className='text-center mt-5'>
      <main className="form-signin w-25 m-auto">
        <form>
          <Logo/>
          <h1 className="h3 mb-3 fw-normal">Please login</h1>
          <ValidationError/>
          <Input label={'Email address'} state={email} setState={setEmail}/>
          <Input label={'Password'} type={'password'} state={password} setState={setPassword}/>

          <button
            className="w-100 btn btn-lg btn-primary"
            type="submit"
            onClick={loginHandler}
          >
            login
          </button>
        </form>
      </main>
    </div>
  );
}

export default Login;