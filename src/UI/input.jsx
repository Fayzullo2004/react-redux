function Input({label, state, setState, type = 'text'}) {
  return (
    <div className="form-floating">
      <input
        type={type}
        className="form-control"
        value={state}
        id="floatingInput"
        placeholder={label}
        onChange={(e) => setState(e.target.value)}
      />
      <label htmlFor="floatingInput">{label}</label>
    </div>
  );
}

export default Input;
